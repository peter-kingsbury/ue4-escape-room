// Copyright (C) 2018, Stormware Studios. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/InputComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEROOMGAME_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
private:
	// How far ahead of the player can we reach in cm?
	float Reach = 100.0f;
	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UInputComponent* InputComponent = nullptr;

	// Find the attached physics handle component
	void FindPhysicsHandleComponent();

	// Setup the (assumed) attached input component
	void FindInputComponent();

	FVector GetReachLineStart();
	FVector GetReachLineEnd();

	// Get the first physics body in reach
	const FHitResult GetFirstPhysicsBodyInReach();
	
	// Raycast and grab what's in reach
	void Grab();

	// Called when grab is released
	void Release();
};
