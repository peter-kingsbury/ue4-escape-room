#!/bin/bash

# 1. Check out or reset to a working commit (use SourceTree)

# 2. Delete derived folders and files
rm -rf .vs .vscode Binaries Build Intermediate Saved *.code-workspace *.xcworkspace *.VC.db

# 3. Reopen Unreal (from launcher or *.uproject
# Build *.dynlib/*.dll files when prompted.

# 4. Regenerate IDE project files.
# Right-click on *.uproject, Services, Generate Xcode Project (even for VSCode!)

